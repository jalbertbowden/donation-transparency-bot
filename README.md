# Donation Transparency Bot

* [Amazon](https://gitlab.com/va-politics/donation-transparency-bot/wikis/amazon-test)
* [AT&T](https://gitlab.com/va-politics/donation-transparency-bot/wikis/AT&T)
* [Comcast](https://gitlab.com/va-politics/donation-transparency-bot/wikis/comcast)
* [Dominion](https://gitlab.com/va-politics/donation-transparency-bot/wikis/dominion)
* [Exxon](https://gitlab.com/va-politics/donation-transparency-bot/wikis/exxon)
* [T-Mobile](https://gitlab.com/va-politics/donation-transparency-bot/wikis/t-mobile)
* [Verizon](https://gitlab.com/va-politics/donation-transparency-bot/wikis/verizon)
* [Walmart](https://gitlab.com/va-politics/donation-transparency-bot/wikis/walmart)



## Reading & Contributing

Currently, it's a little on the rough end; there's too many magic values. Also, be aware that the Dept of Elections spider is both on-the-fly and NOT polite. Take this in mind; don't accidently DDoS them.
 
 
## License

![License](https://gitlab.com/va-politics/donation-transparency-bot/raw/master/docs/license.png)
