package com.gitlab.ccook;

import com.gitlab.ccook.model.GAMember;
import com.gitlab.ccook.util.BotUtils;
import com.gitlab.ccook.util.GitlabWikiReader;
import com.gitlab.ccook.util.Option;
import twitter4j.Status;

import java.util.*;

public class TweetComposer {

    private List<String> tweets = new ArrayList<>();

    public TweetComposer(String wikiSlug,
                         String companyHandle,
                         int topNumberToReport,
                         Option<String> additionalMedia) throws BotException {
        GitlabWikiReader reader = new GitlabWikiReader();
        Map<GAMember, Double> read = reader.read(wikiSlug);
        String header = "Since 2012, "+companyHandle+" gave ";
        List<GAMember> reversableStack = new Stack<>();
        double total = 0;
        read = sortByValue(read);
        for (Map.Entry<GAMember, Double> ent : read.entrySet()) {
            reversableStack.add(ent.getKey());
            total += ent.getValue();
        }
        header += "$"+total+" to "+read.size()+" members of the General Assembly.\n";
        Collections.reverse(reversableStack);
        tweets = new ArrayList<>();
        header += "\nAll contributions: https://gitlab.com/va-politics/donation-transparency-bot/wikis/"+wikiSlug;
        header += "\nTop "+topNumberToReport+" contribution receivers:\n";
        Status lastTweet = BotUtils.tweet(header, new Option<>());
        for (int i = 0; i < topNumberToReport; i++) {
            GAMember member = reversableStack.get(i);
            String tweet = (i+1)+".\t "+member.getName();
            if (member.getTwitterHandle().isDefined()) {
                tweet += " (@" + member.getTwitterHandle().get() + ")";
            }
            tweet +=" received $"+read.get(member)+" from "+companyHandle+" since 2012.\n";
            lastTweet = BotUtils.tweet(tweet, new Option<>(lastTweet.getId()));
              //  lastTweet = BotUtils.tweet(tweet, new Option<>(lastTweet.getId()));
        }
        for(String t: tweets) {
            System.out.println("T:"+t);
        }
    }

    public List<String> getTweets() {
        return tweets;
    }

    public static void main(String[] args) throws Exception {
        TweetComposer composer = new TweetComposer("comcast", "@comcast",10, new Option<>());
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }


}
