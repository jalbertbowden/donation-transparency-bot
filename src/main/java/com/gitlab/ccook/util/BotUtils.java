package com.gitlab.ccook.util;

import com.gitlab.ccook.BotException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import twitter4j.*;

import java.util.concurrent.TimeUnit;

public class BotUtils {


    public static DateTime parseDate(String trim) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime parse = DateTime.parse(trim, fmt);
        return parse;
    }

    public static Status tweet(String text, Option<Long> inReplyTo) throws BotException {
        Twitter twitter = TwitterFactory.getSingleton();
        StatusUpdate b = new StatusUpdate(text);
        if(inReplyTo.isDefined()){
            b.inReplyToStatusId(inReplyTo.get());
        }
        try {
            Status status = twitter.updateStatus(b);
            try {
                TimeUnit.SECONDS.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return status;
        } catch (TwitterException e) {
            throw new BotException("Tweet failed",e);
        }
    }

    public static void testTwitter() throws Exception{
        Twitter twitter = TwitterFactory.getSingleton();
        Status status = twitter.updateStatus("MAINTENANCE: this is a test of the bot's connection to twitter.");
        StatusUpdate b = new StatusUpdate("MAINTENANCE: this is a test of the bot's ability to thread tweets.");
        b.inReplyToStatusId(status.getId());
        twitter.updateStatus(b);
        System.out.println("Successfully updated the status to [" + status.getText() + "].");
    }
}
