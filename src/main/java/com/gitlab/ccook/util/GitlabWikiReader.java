package com.gitlab.ccook.util;

import com.gitlab.ccook.BotException;
import com.gitlab.ccook.GAMemberFinder;
import com.gitlab.ccook.model.GAMember;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class GitlabWikiReader {

    public String wikiPath = "donation-transparency-bot.wiki/";

    public Map<GAMember, Double> read(String companySlug) throws BotException {
        try {
            Map<GAMember, Double> memberToContributions = new HashMap<>();
            File f = new File(wikiPath + companySlug + ".md");
            Scanner in = new Scanner(new File(wikiPath + companySlug + ".md"));
            in.useDelimiter("\n");
            String member = "";
            double total = 0;
            while (in.hasNext()) {
                String line = in.nextLine();
                if (line.startsWith("##")) {
                    if (!member.isEmpty() && total != 0) {
                        memberToContributions.put(findMember(member), total);
                    }
                    member = line.trim().replace("#", "");
                    total = 0;
                } else if (line.startsWith("|") && !line.contains("Transaction Date") && !line.contains("| -- |")) {
                    //| 2018-10-29T00:00:00.000-04:00 | Amazon PAC | P O Box 80683 Seattle WA 98108 | 250.0 | 250.0 |
                    String[] parts = line.split("\\|");
                    total += Double.parseDouble(parts[4].trim());
                    //rawDonationLines.add(toPair(line));
                }
            }
            if (total != 0) {
                memberToContributions.put(findMember(member), total);
            }
            return memberToContributions;
        } catch (FileNotFoundException e) {
            throw new BotException("Couldnt find wiki", e);
        }
    }


    private GAMember findMember(String member) throws BotException {
        GAMemberFinder finder = new GAMemberFinder();
        for (GAMember finderMember : finder.getMembers()) {
            if (finderMember.getName().trim().equalsIgnoreCase(member.trim())) {
                return finderMember;
            }
        }
        throw new BotException("Cound not find " + member);
    }
}
