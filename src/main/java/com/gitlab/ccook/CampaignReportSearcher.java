package com.gitlab.ccook;

import com.gitlab.ccook.model.CampaignFinanceReport;
import com.gitlab.ccook.model.CommitteeSearchResult;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CampaignReportSearcher {

    private String committeeName;
    private static Logger log = LoggerFactory.getLogger(CampaignReportSearcher.class);

    public CampaignReportSearcher(String comName) {
        this.committeeName = comName;
        log.info("Comm name:"+comName);
    }

    public CommitteeSearchResult getCommitteeInfo() throws BotException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://cfreports.sbe.virginia.gov/Home/SearchCommittees");

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("partialCommitteeName", committeeName));
        params.add(new BasicNameValuePair("CommitteeType", ""));
        params.add(new BasicNameValuePair("page", "1"));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            CloseableHttpResponse response = client.execute(httpPost);
            String s = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
            Gson gson = new Gson();

            CommitteeSearchResult result = gson.fromJson(s, CommitteeSearchResult.class);
            return result;
        } catch (IOException e) {
            throw new BotException("Get Committee Info failed.", e);
        } finally {
            IOUtils.closeQuietly(client);
        }
    }

    public List<CampaignFinanceReport> getCampaignFinanceInfo(String campaignAccountId) throws BotException {
        //http://cfreports.sbe.virginia.gov/Committee/Index/c1d29300-29cc-e111-b0b8-984be103f032
        try {
            List<CampaignFinanceReport> toReturn = new ArrayList<>();
            Document doc = Jsoup.parse(new URL("http://cfreports.sbe.virginia.gov/Committee/Index/" + campaignAccountId), 1 * 60 * 1000);
            //ScheduledReports
            Elements reports = doc.select("[title=Click to view report]");
            Iterator<Element> iterator = reports.iterator();
            while (iterator.hasNext()) {
                Element next = iterator.next();
                String link = next.attr("href").replace("/Report/Index/", "/Report/ReportXML/");
                System.out.println(link);
                toReturn.add(new CampaignFinanceReport(link));
            }
            return toReturn;
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }


}
