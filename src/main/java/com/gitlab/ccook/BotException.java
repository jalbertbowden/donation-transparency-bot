package com.gitlab.ccook;

public class BotException extends Exception {

    public BotException(String msg, Throwable e) {
        super(msg, e);
    }

    public BotException(String s) {
        super(s);
    }
}
