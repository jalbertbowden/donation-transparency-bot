package com.gitlab.ccook.model;

import com.gitlab.ccook.util.Option;

import java.util.Objects;

public class GAMember {
    private String link;
    private Option<String> twitterHandle = new Option<>();
    private String name;
    private String slug;

    public GAMember(String name, Option<String> twitterHandle, String link) {
        this.link =  link;
        this.name = name;
        this.twitterHandle = twitterHandle;
        slug = name.replace(".","")
                .replace(" ","-").replace(",","").replace("\"","").toLowerCase();

    }

    public Option<String> getTwitterHandle() {
        return twitterHandle;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public String getSlug() {
        return slug;
    }

    @Override
    public String toString() {
        return "GAMember{" +
                "link='" + link + '\'' +
                ", twitterHandle=" + twitterHandle +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GAMember gaMember = (GAMember) o;
        return Objects.equals(link, gaMember.link) &&
                Objects.equals(twitterHandle, gaMember.twitterHandle) &&
                Objects.equals(name, gaMember.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(link, twitterHandle, name);
    }
}
