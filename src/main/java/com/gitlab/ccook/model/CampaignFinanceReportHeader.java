package com.gitlab.ccook.model;

import com.gitlab.ccook.util.BotUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class CampaignFinanceReportHeader {
    private static Logger log = LoggerFactory.getLogger(CampaignFinanceReportHeader.class);
    private String committeeCode;
    private String committeeName;
    private String reportYear;
    private CampaignFinanceAddress address;
    private DateTime filingDate;
    private DateTime startDate;
    private DateTime endDate;
    private String submitterPhone;
    private String submitterEmail;
    private String filingType;
    private boolean isFinalReport;
    private boolean isAmendment;
    private Integer amendedReportNumber;
    private boolean noActivity;
    private Double balanceSinceLastReportingPeriod;
    private String electionCycle;
    private String officeSought;
    private String district;

    public CampaignFinanceReportHeader(Element reportHeader) {
        Elements committeeCode = reportHeader.select("committeecode");
        if (committeeCode.iterator().hasNext()) {
            this.committeeCode = committeeCode.iterator().next().text().trim();
        }
        Elements committeeName = reportHeader.select("CommitteeName");
        if (committeeName.iterator().hasNext()) {
            this.committeeName = committeeName.iterator().next().text().trim();
        }
        Elements reportYear = reportHeader.select("ReportYear");
        if (reportYear.iterator().hasNext()) {
            this.reportYear = reportYear.iterator().next().text().trim();
        }
        Elements address = reportHeader.select("Address");
        if (address.iterator().hasNext()) {
            this.address = new CampaignFinanceAddress(address);
        }
        Elements filingDate = reportHeader.select("FilingDate");
        if (filingDate.iterator().hasNext()) {
            this.filingDate = BotUtils.parseDate(filingDate.iterator().next().text().trim());
        }
        Elements startDate = reportHeader.select("StartDate");
        if (startDate.iterator().hasNext()) {
            this.startDate = BotUtils.parseDate(startDate.iterator().next().text().trim());
        }
        Elements endDate = reportHeader.select("EndDate");
        if (endDate.iterator().hasNext()) {
            this.endDate = BotUtils.parseDate(endDate.iterator().next().text().trim());
        }
        Elements submitterPhone = reportHeader.select("SubmitterPhone");
        if (submitterPhone.iterator().hasNext()) {
            this.submitterPhone = submitterPhone.iterator().next().text().trim();
        }
        Elements submitterEmail = reportHeader.select("SubmitterEmail");
        if (submitterEmail.iterator().hasNext()) {
            this.submitterEmail = submitterEmail.iterator().next().text().trim();
        }
        Elements filingType = reportHeader.select("FilingType");
        if (filingType.iterator().hasNext()) {
            this.filingType = filingType.iterator().next().text().trim();
        }
        Elements isFinalReport = reportHeader.select("IsFinalReport");
        if (isFinalReport.iterator().hasNext()) {
            this.isFinalReport = Boolean.parseBoolean(isFinalReport.iterator().next().text().trim());
        }
        Elements isAmendment = reportHeader.select("IsAmendment");
        if (isAmendment.iterator().hasNext()) {
            this.isAmendment = Boolean.parseBoolean(isAmendment.iterator().next().text().trim());
        }
        Elements amendedReportNumber = reportHeader.select("AmendedReportNumber");
        if (amendedReportNumber.iterator().hasNext()) {
            this.amendedReportNumber = Integer.parseInt(amendedReportNumber.iterator().next().text().trim());
        }
        Elements noActivity = reportHeader.select("NoActivity");
        if (noActivity.iterator().hasNext()) {
            this.noActivity = Boolean.parseBoolean(noActivity.iterator().next().text().trim());
        }
        Elements balanceLastReportingPeriod = reportHeader.select("BalanceLastReportingPeriod");
        if (balanceLastReportingPeriod.iterator().hasNext()) {
            this.balanceSinceLastReportingPeriod = Double.parseDouble(balanceLastReportingPeriod.iterator().next().text().trim());
        }
        Elements electionCycle = reportHeader.select("ElectionCycle");
        if (electionCycle.iterator().hasNext()) {
            this.electionCycle = electionCycle.iterator().next().text().trim();
        }
        Elements officeSought = reportHeader.select("OfficeSought");
        if (officeSought.iterator().hasNext()) {
            this.officeSought = officeSought.iterator().next().text().trim();
        }
        Elements district = reportHeader.select("District");
        if (district.iterator().hasNext()) {
            this.district = district.iterator().next().text().trim();
        }

    }

    @Override
    public String toString() {
        return "CampaignFinanceReportHeader{" +
                "committeeCode='" + committeeCode + '\'' +
                ", committeeName='" + committeeName + '\'' +
                ", reportYear='" + reportYear + '\'' +
                ", address=" + address +
                ", filingDate=" + filingDate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", submitterPhone='" + submitterPhone + '\'' +
                ", submitterEmail='" + submitterEmail + '\'' +
                ", filingType='" + filingType + '\'' +
                ", isFinalReport=" + isFinalReport +
                ", isAmendment=" + isAmendment +
                ", amendedReportNumber=" + amendedReportNumber +
                ", noActivity=" + noActivity +
                ", balanceSinceLastReportingPeriod=" + balanceSinceLastReportingPeriod +
                ", electionCycle='" + electionCycle + '\'' +
                ", officeSought='" + officeSought + '\'' +
                ", district='" + district + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignFinanceReportHeader that = (CampaignFinanceReportHeader) o;
        return isFinalReport == that.isFinalReport &&
                isAmendment == that.isAmendment &&
                noActivity == that.noActivity &&
                Objects.equals(committeeCode, that.committeeCode) &&
                Objects.equals(committeeName, that.committeeName) &&
                Objects.equals(reportYear, that.reportYear) &&
                Objects.equals(address, that.address) &&
                Objects.equals(filingDate, that.filingDate) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(submitterPhone, that.submitterPhone) &&
                Objects.equals(submitterEmail, that.submitterEmail) &&
                Objects.equals(filingType, that.filingType) &&
                Objects.equals(amendedReportNumber, that.amendedReportNumber) &&
                Objects.equals(balanceSinceLastReportingPeriod, that.balanceSinceLastReportingPeriod) &&
                Objects.equals(electionCycle, that.electionCycle) &&
                Objects.equals(officeSought, that.officeSought) &&
                Objects.equals(district, that.district);
    }

    @Override
    public int hashCode() {
        return Objects.hash(committeeCode, committeeName, reportYear, address, filingDate, startDate, endDate, submitterPhone, submitterEmail, filingType, isFinalReport, isAmendment, amendedReportNumber, noActivity, balanceSinceLastReportingPeriod, electionCycle, officeSought, district);
    }

    public static Logger getLog() {
        return log;
    }

    public String getCommitteeCode() {
        return committeeCode;
    }

    public String getCommitteeName() {
        return committeeName;
    }

    public String getReportYear() {
        return reportYear;
    }

    public CampaignFinanceAddress getAddress() {
        return address;
    }

    public DateTime getFilingDate() {
        return filingDate;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public String getSubmitterPhone() {
        return submitterPhone;
    }

    public String getSubmitterEmail() {
        return submitterEmail;
    }

    public String getFilingType() {
        return filingType;
    }

    public boolean isFinalReport() {
        return isFinalReport;
    }

    public boolean isAmendment() {
        return isAmendment;
    }

    public Integer getAmendedReportNumber() {
        return amendedReportNumber;
    }

    public boolean isNoActivity() {
        return noActivity;
    }

    public Double getBalanceSinceLastReportingPeriod() {
        return balanceSinceLastReportingPeriod;
    }

    public String getElectionCycle() {
        return electionCycle;
    }

    public String getOfficeSought() {
        return officeSought;
    }

    public String getDistrict() {
        return district;
    }
}
