package com.gitlab.ccook.model;

import com.gitlab.ccook.util.Pair;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CampaignFinanceReportScheduleA {
    private static Logger log = LoggerFactory.getLogger(CampaignFinanceReportScheduleA.class);
    private List<Pair<Contributor, TransactionData>> contributions = new ArrayList<>();
    public CampaignFinanceReportScheduleA(Element scheduleA) {
        Elements liA = scheduleA.select("LiA");
        Iterator<Element> iterator = liA.iterator();
        while (iterator.hasNext()) {
            Element next = iterator.next();
            contributions.add(new Pair<>(new Contributor(next.select("Contributor")), new TransactionData(next)));
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampaignFinanceReportScheduleA that = (CampaignFinanceReportScheduleA) o;
        return Objects.equals(contributions, that.contributions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contributions);
    }

    @Override
    public String toString() {
        return "CampaignFinanceReportScheduleA{" +
                "contributions=" + contributions +
                '}';
    }

    public List<Pair<Contributor, TransactionData>> getContributions() {
        return contributions;
    }
}
