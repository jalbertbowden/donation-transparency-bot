package com.gitlab.ccook.model;

import java.util.List;
import java.util.Objects;

public class CommitteeSearchResult {
    public int RecordCount;
    public int Page;
    public int PageSize;
    public List<Committee> Committees;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommitteeSearchResult that = (CommitteeSearchResult) o;
        return RecordCount == that.RecordCount &&
                Page == that.Page &&
                PageSize == that.PageSize &&
                Objects.equals(Committees, that.Committees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(RecordCount, Page, PageSize, Committees);
    }


    @Override
    public String toString() {
        return "CommitteeSearchResult{" +
                "RecordCount=" + RecordCount +
                ", Page=" + Page +
                ", PageSize=" + PageSize +
                ", Committees=" + Committees +
                '}';
    }

    public int getRecordCount() {
        return RecordCount;
    }

    public void setRecordCount(int recordCount) {
        RecordCount = recordCount;
    }

    public int getPage() {
        return Page;
    }

    public void setPage(int page) {
        Page = page;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public List<Committee> getCommittees() {
        return Committees;
    }

    public void setCommittees(List<Committee> committees) {
        Committees = committees;
    }
}
